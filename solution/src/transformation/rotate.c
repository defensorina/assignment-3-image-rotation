#include "rotate.h"

void rotate(struct image *img,  struct  image *img_rotate)
{
    allocate_image(img_rotate, img->height, img->width);
    
    for (uint64_t x = 0; x < img->width; ++x)
    {
        for (uint64_t y = 0; y < img->height; ++y)
        {
            img_rotate->data[img_rotate->width * x + y] = img->data[(img->height - y - 1) * img->width + x];
        }
    }
}

