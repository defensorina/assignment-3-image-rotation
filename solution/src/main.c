#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "transformation/bmp.h"
#include "transformation/rotate.h"

char *get_read_error(enum read_status st)
{
    switch(st)
    {
        case READ_FILE_NOT_FOUND: return "not found file";
        case READ_INVALID_HEADER: return "invalid header";
        case READ_INVALID_BITS: return "invalid bits";
        case READ_INVALID_SIGNATURE: return "invalid signature";
        default: return "success";
    }
}
char *get_write_error(enum write_status st)
{
    switch(st)
    {
        case WRITE_ERROR: return "writting error";
        case WRITE_FILE_NOT_FOUND: return "not found file";
        default: return "success";
    }
}
int main(int argc, char **argv)
{
    if (argc != 3) {
        fprintf(stderr, "needs 3 arguments");
        return 1;
    }

    struct image img = {0};

    enum read_status read_status = open_bmp_file(argv[1], &img);
    if (read_status != READ_OK) {
        fprintf(stderr, "%s\n", get_read_error(read_status));
        free_image(&img);
        return 1;
    }
    
    struct image img_rotate = {0};
    rotate(&img, &img_rotate);
    
    enum write_status write_status = close_bmp_file(argv[2], &img_rotate);
    if (write_status != WRITE_OK) {
        fprintf(stderr, "%s\n", get_write_error(write_status));
        free_image(&img);
        free_image(&img_rotate);
        return 1;
    }

    free_image(&img);
    free_image(&img_rotate);

    return 0;
}
